jQuery(document).ready(function(){
  
  $(".close").click(function(){
    $(this).parent().animate({height: '0', padding:'0 20px', opacity:'0'},200);
  });
  var currentScroll;
  $(".toggle").click(function(){
    $(".menu-c1").toggle(function(){
      $(".menu-c1").css({
        opacity:'1'
      });
    });
    currentScroll = $(window).scrollTop();
  });

  $(window).scroll(function(){
    var afterScroll = $(window).scrollTop();
    if($(window).width()<=1200){
      if(afterScroll != currentScroll){
        $(".menu-c1").hide(250);
      }
    }
  });

  $(".show-search").click(function(){
    $(".search-1").show(function(){
      $(".search-1").css({'opacity':'1'});
    });
  });

  $(".close-search").click(function(){
    $(".search-1").hide(function(){
      $(".search-1").css({'opacity':'0'});
    });
  });
  
  $('.slide-banner').owlCarousel({
    owlCarousel({
      loop:true,
      nav:true,
      autoplay: true,
      autoplayTimeout: 10000,
      autoplayHoverPause: true,
      responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
    }
});

  $('.selecting .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items: 1
  });
});
